////  FruitModel.swift
//  SwiftUI2_Fructus
//
//  Created on 19/01/2021.
//  
//

import Foundation
import SwiftUI

// MARK: FRUITS MODEL

struct Fruit: Identifiable {
    var id = UUID()
    var title: String
    var headline: String
    var image: String
    var gradientColors: [Color]
    var description : String
    var nutrition: [String]
}
