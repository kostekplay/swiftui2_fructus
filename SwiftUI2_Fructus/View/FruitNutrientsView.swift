////  FruitNutrientsView.swift
//  SwiftUI2_Fructus
//
//  Created on 21/01/2021.
//  
//

import SwiftUI

struct FruitNutrientsView: View {
    
    // MARK: - PROPERTIES
    var fruit: Fruit
    let nutriens: [String] = [
        "Energy",
        "Sugar",
        "Fat",
        "Protein",
        "Vitamins",
        "Minerals"
    ]
    
    // MARK: - BODY
    var body: some View {
        GroupBox(){
            DisclosureGroup(
                content: {
                    ForEach(0..<nutriens.count, id: \.self) { item in
                        
                        Divider().padding(.vertical, 2)
                        
                        HStack {
                            Group {
                                Image(systemName: "info.circle")
                                Text(nutriens[item])
                            }
                            .foregroundColor(fruit.gradientColors[1])
                            .font(Font.system(.body).bold())
                            Spacer(minLength: 25)
                            Text(fruit.nutrition[item])
                                .multilineTextAlignment(.trailing)
                        } //: HStack
                    }},
                label: { Text("Nutritional value per 100g") }
            ) //: DisclousureGroup
        } //:GroupBox
    }
}

// MARK: - PREVIEW
struct FruitNutrientsView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            FruitNutrientsView(fruit: fruitsData[0])
                .previewLayout(.fixed(width: 375, height: 480))
                .padding()
            FruitNutrientsView(fruit: fruitsData[0])
                .preferredColorScheme(.dark)
                .previewLayout(.fixed(width: 375, height: 480))
                .padding()
        }
    }
}
