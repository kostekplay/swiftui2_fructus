////  StartButtonView.swift
//  SwiftUI2_Fructus
//
//  Created on 19/01/2021.
//  
//

import SwiftUI

struct StartButtonView: View {
    
    // MARK: PREPERTIES
    @AppStorage("isOnbording") var isOnbording: Bool?
    
    // MARK: BODY
    var body: some View {
        Button(action: {
            isOnbording = false
        }, label: {
            HStack(spacing: 8) {
                Text("Start")
                Image(systemName: "arrow.right.circle")
                    .imageScale(.large)
            } //:HStack
            .padding(.horizontal, 16)
            .padding(.vertical,10)
            .background(Capsule().strokeBorder(Color.white, lineWidth: 1.25))
        })
    }
}

// MARK: PREVIEW
struct StartButtonView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            StartButtonView()
                .previewLayout(.sizeThatFits)
            StartButtonView()
                .preferredColorScheme(.dark)
                .previewLayout(.sizeThatFits)
        }
    }
}
