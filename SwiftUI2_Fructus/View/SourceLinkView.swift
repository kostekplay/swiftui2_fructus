////  SourceLinkView.swift
//  SwiftUI2_Fructus
//
//  Created on 21/01/2021.
//  
//

import SwiftUI

struct SourceLinkView: View {
    var body: some View {
        GroupBox() {
            HStack {
                Text("Conten source")
                Spacer()
//                Link(destination: URL(string: "https:\\wikipedia.com")!, label: {
//                    Text("Wikipedia")
//                })
                Link("Wikipedia", destination: URL(string: "https://wikipedia.com")!)
                Image(systemName: "arrow.up.right.square")
            } //: HStack
            .font(.footnote)
        } //: GroupBox
    }
}

struct SourceLinkView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SourceLinkView()
                .previewLayout(.sizeThatFits)
                .padding()
            SourceLinkView()
                .preferredColorScheme(.dark)
                .previewLayout(.sizeThatFits)
                .padding()
        }
    }
}
