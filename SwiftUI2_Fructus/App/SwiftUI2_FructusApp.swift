////  SwiftUI2_FructusApp.swift
//  SwiftUI2_Fructus
//
//  Created on 18/01/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_FructusApp: App {
    
    @AppStorage("isOnbording") var isOnbording: Bool = false
    
    var body: some Scene {
        WindowGroup {
            if isOnbording {
                OnboardingView()
            } else {
                ContentView()
            }
        }
    }
}
