////  OnboardingView.swift
//  SwiftUI2_Fructus
//
//  Created on 19/01/2021.
//  
//

import SwiftUI

struct OnboardingView: View {
    
    // MARK: PREPERTIES
    var fruits: [Fruit] = fruitsData
    
    // MARK: BODY
    var body: some View {
        TabView{
            ForEach(0..<fruitsData.count){ item in
                FruitCardView(fruit: fruitsData[item])
            } //: ForEach
        } //: TabView
        .tabViewStyle(PageTabViewStyle())
        .padding(.vertical, 20)
    }
}

// MARK: PREVIEW
struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView(fruits: fruitsData)
    }
}
